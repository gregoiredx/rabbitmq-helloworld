# RabbitMQ Hello World

Implements  https://www.rabbitmq.com/tutorials/tutorial-one-python.html in `docker-compose`.

Run it with:
```
docker-compose up --build
```

Send custom messages to the consumer:
```
docker-compose run producer python producer.py bla blu bli
```
