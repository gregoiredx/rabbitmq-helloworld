FROM python:3.9-alpine AS common
ENV PYTHONUNBUFFERED=1
COPY wait-for .
COPY requirements.txt .
RUN pip install --no-cache-dir --upgrade -r requirements.txt

FROM common AS producer
COPY producer.py .
CMD ./wait-for rabbitmq:5672 -- python producer.py

FROM common AS consumer
COPY consumer.py .
CMD ./wait-for rabbitmq:5672 -- python consumer.py
