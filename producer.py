import pika
import sys
import time

with  pika.BlockingConnection(pika.ConnectionParameters('rabbitmq')) as connection:
    channel = connection.channel()
    channel.queue_declare(queue='hello')

    messages = sys.argv[1:] or [f'Hello World {i}!' for  i in range(10)]

    for message in messages:
        channel.basic_publish(exchange='', routing_key='hello', body=message)
        print(f" [x] Sent '{message}'")
        time.sleep(1)
