import pika


def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)

with  pika.BlockingConnection(pika.ConnectionParameters('rabbitmq')) as connection:
    channel = connection.channel()
    channel.queue_declare(queue='hello')

    channel.basic_consume(queue='hello', auto_ack=True, on_message_callback=callback)
    print(' [*] Waiting for messages. To exit press CTRL+C')

    channel.start_consuming()
